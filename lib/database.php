<?php

//$db = new mysqli('localhost', 'telegram', '01490848', 'telegram');
//$db->set_charset('utf8');
//
//
//if ($result = $db->query('SELECT * FROM `users`')) {
//
//
//	while ($row = $result->fetch_assoc()) {
//		print_r($row);
//	}
//	$result->free_result();
//}
//$db->close();


class lib_database {
	/**
	 * @var mysqli $_instance
	 */
	private static $_instance = null;

	private function __construct() {
	}

	public function connect($host, $username, $passwd, $dbname){
		self::$_instance = new mysqli($host, $username, $passwd, $dbname);
		self::$_instance->set_charset('utf8');
	}


	/**
	 * @return mysqli
	 */
	static public function getInstance() {
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}

}