<?php

/**
 * Created by PhpStorm.
 * User: Валера
 * Date: 03.06.2016
 * Time: 21:45
 */
class model_users_user extends model_base{
	/** хранит свойства объекта
	 * @var stdClass $fields
	 */
	protected $fields;

	public function __construct($id) {
		parent::__construct();
		$this->fields = $this->db->query('SELECT * FROM `users` WHERE `id` = ' . (int)$id)->fetch_object();
		if(!isset($this->fields->id)) throw new Exception("Пользователь не существует");
	}

	public function getId(){
		return $this->fields->id;
	}

	public function updateModifyTime(){
		$this->fields->modify = date('Y-m-d H:i:s');
	}

	public function getField($fieldName = NULL){
		if(is_null($fieldName)) return $this->fields;
		else if(isset($this->fields->$fieldName)) return $this->fields->$fieldName;
		else return false;
	}

	public function setField($name, $value) {
		if(isset($this->fields->$name)){
			// TODO: Запилить проверку входных данных
			$this->fields->$name = $value;
			return true;
		}else{
			return false;
		}
	}

	public function save(){
		$saveArray = array();
		foreach($this->fields as $fieldName=>$fieldValue){
			if($fieldName == 'id') continue;
			$saveArray[] = "`{$fieldName}` = '{$fieldValue}'";
		}

		$data = implode(', ',$saveArray);

		$this->updateModifyTime();
		return $this->db->query("UPDATE `users` SET {$data} WHERE `id` = " . $this->getId());
	}
}