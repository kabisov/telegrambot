<?php

/**
 * Created by PhpStorm.
 * User: Валера
 * Date: 03.06.2016
 * Time: 20:59
 */
class model_users_collection extends model_base implements implements_singleton, implements_collection{
	protected static $_instance = null;

	public function __construct() {
		parent::__construct();
	}

	/**
	 * @param $id id пользователя
	 * @return model_users_user|bool
	 */
	public function getById($id) {
		try{
			return new model_users_user($id);
		}catch(Exception $e){
			return false;
		}
	}

	public function add($id){
		$this->db->query('INSERT INTO `users` SET `id`=' . (int)$id);
		return $this->getById($id);
	}

	public function getAll() {
		$users = array();
		foreach($this->db->query("SELECT `id` FROM `users`")->fetch_all(MYSQLI_ASSOC) as $userId){
			$users[] = $this->getById(current($userId));
		}
		return $users;
	}

	public static function getInstance() {
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
}