<?php
/**
 * Created by PhpStorm.
 * User: Валера
 * Date: 05.06.2016
 * Time: 22:35
 */

class model_messages_collection extends model_base implements implements_singleton, implements_collection{
	protected static $_instance = null;

	public function getAll() {
		// TODO: Implement getAll() method.
	}

	public function __construct() {
		parent::__construct();
	}

	public static function getInstance() {
		if (is_null(self::$_instance)) {
			self::$_instance = new self();
		}
		return self::$_instance;
	}
}